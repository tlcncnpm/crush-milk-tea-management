/* eslint-disable no-restricted-syntax */
import jQuery from 'jquery';
import { STRING } from '../constants/Const';

/**
 * Convert object to URI format
 * @param {object} uriObject
 */
// eslint-disable-next-line import/prefer-default-export
export function formatURI(uriObject) {
  for (const key in uriObject) {
    if (!uriObject[key]) {
      // delete uriObject[key]
    }
  }
  // uriObject = JSON.parse(JSON.stringify(uriObject));
  return STRING.QUESTION_MARK + jQuery.param(uriObject);
}
