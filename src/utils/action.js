export default function action(type) {
  return function data(payload = {}, meta = {}) {
    return {
      type,
      payload,
      meta,
    };
  };
}
