import { STRING } from '../constants/Const';

/**
 * Convert hex to base64
 */
export function hexToBase64(hexstring) {
  return btoa(
    hexstring
      .match(/\w{2}/g)
      .map(a => String.fromCharCode(parseInt(a, 16)))
      .join(STRING.EMPTY),
  );
}

/**
 * Convert file to base 64
 * @param {file} file
 */
export function getBase64FromFile(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

/**
 * Convert file to base 64
 * @param {file} file
 */
export function getTextFromFile(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsText(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

/**
 * Convert file image to base 64
 * @param {file} file
 */
export function getBase64FromIMG(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

/**
 * Convert string to base64
 * @param {string} string
 * @returns {string} base64
 */
export function encryptStringToBase64(string) {
  return btoa(string);
}
