/* eslint-disable no-param-reassign */
import moment from 'moment';
import { FORMAT_DATE, STRING } from '../constants/Const';

/**
 * Convert Date to timestamp
 * @param {date} dateForm
 * @param {date} dateTo
 * @returns {object}
 */
export function convertDateToTimestamp(dateForm, dateTo) {
  if (dateTo) {
    dateTo = moment(
      moment(dateTo).format(FORMAT_DATE.DATE) + FORMAT_DATE.TIME_END_DAY,
      FORMAT_DATE.DATE_TIME,
    ).unix();
  } else {
    dateTo = STRING.EMPTY;
  }
  if (dateForm) {
    dateForm = moment(
      moment(dateForm).format(FORMAT_DATE.DATE) + FORMAT_DATE.TIME_BEGIN_DAY,
      FORMAT_DATE.DATE_TIME,
    ).unix();
  } else {
    dateForm = STRING.EMPTY;
  }
  return { dateTo, dateForm };
}

/**
 * Convert Date to mstimestamp
 * @param {date} dateForm
 * @param {date} dateTo
 * @returns {object}
 */
export function convertDateToMSTimestamp(dateForm, dateTo) {
  if (dateTo) {
    dateTo = moment(
      moment(dateTo).format(FORMAT_DATE.DATE) + FORMAT_DATE.TIME_END_DAY,
      FORMAT_DATE.DATE_TIME,
    ).valueOf();
  } else {
    dateTo = STRING.EMPTY;
  }
  if (dateForm) {
    dateForm = moment(
      moment(dateForm).format(FORMAT_DATE.DATE) + FORMAT_DATE.TIME_BEGIN_DAY,
      FORMAT_DATE.DATE_TIME,
    ).valueOf();
  } else {
    dateForm = STRING.EMPTY;
  }
  return { dateTo, dateForm };
}

/**
 * Convert timestamp (seconds) to localtime
 * @param {number} timestamp
 * @returns {number} timestamp
 */
export function timestampToLocalTime(timestamp) {
  return moment.unix(timestamp).format(FORMAT_DATE.DATE_TIME);
}

/**
 * Convert timestamp (milliseconds) to localtime
 * @param {number} timestamp
 * @param {string} formatDate
 *
 */
export function msTimestampToLocalTime(timestamp, formatDate) {
  return moment(timestamp).format(formatDate || FORMAT_DATE.DATE_TIME);
}

/**
 * Convert timestamp (milliseconds) to local date
 * @param {number} timestamp
 */
export function msTimestampToLocalDate(timestamp) {
  if (!timestamp) {
    return null;
  }
  return moment(timestamp).format(FORMAT_DATE.DATE);
}
