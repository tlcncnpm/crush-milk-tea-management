import { RSAKey, JSEncrypt } from 'jsencrypt';

/**
 * Encrypt data with mod and expo
 * @param {*} mod
 * @param {*} expo
 * @param {string} data
 * @returns {string} data encrypt
 */
export function rsaEncrypt(mod, expo, data) {
  const rsa = new RSAKey();
  rsa.setPublic(mod, expo);
  return rsa.encrypt(data);
}

/**
 * Generate Key
 * @param {number} keySize // default 1024
 * @returns {object} private Key and public Key
 */
export function generateKey(keySize) {
  const option = {
    // eslint-disable-next-line camelcase
    default_key_size: keySize,
  };
  const crypt = new JSEncrypt(option);
  crypt.getKey();
  const privateKey = crypt.getPrivateKey();
  const publicKey = crypt.getPublicKey();
  return { privateKey, publicKey };
}

/**
 * Decrypt data
 * @param {number} keySize // default 1024
 * @param {string} privateKey
 * @param {string} data // base64
 * @returns {string} data is decrypt
 */
export function decryptData(keySize, privateKey, data) {
  const option = {
    // eslint-disable-next-line camelcase
    default_key_size: keySize,
  };
  const crypt = new JSEncrypt(option);
  crypt.setPrivateKey(privateKey);
  return crypt.decrypt(data);
}
