/**
 * Convert an Object -> only include Object have value is true
 */
export const onConvertToObjectTrue = data => Object.keys(data).filter(key => data[key]);

/**
 * Convert an Object -> only include Object have value is false
 */
export const onConvertToObjectFalse = data => Object.keys(data).filter(key => !data[key]);
