// import { STRING } from "constants/Const"

/**
 * Convert base64 to string
 * @param {string} base64
 * @returns {string} data is decrypt
 */
export function decryptBase64ToString(base64) {
  return atob(base64);
}

export function parseJwt(token) {
  const base64Url = token.split('.')[1];
  const base64 = decodeURIComponent(
    atob(base64Url)
      .split('')
      .map(c => `%${(`00${c.charCodeAt(0).toString(16)}`).slice(-2)}`)
      .join(''),
  );

  return JSON.parse(base64);
}
