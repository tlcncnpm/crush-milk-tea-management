/**
 * Function to pause execution for a fixed amount of milliseconds.
 * @param {number} milliseconds
 */
// eslint-disable-next-line import/prefer-default-export
export const sleep = milliseconds => new Promise(resolve => setTimeout(resolve, milliseconds));
