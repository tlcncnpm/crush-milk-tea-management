import { authentication } from './types';
import action from '../utils/action';

export const registerRequest = action(authentication.REGISTER_REQUEST);
export const registerSuccess = action(authentication.REGISTER_SUCCESS);
export const registerFailure = action(authentication.REGISTER_FAILURE);

export const loginRequest = action(authentication.LOGIN_REQUEST);
export const loginSuccess = action(authentication.LOGIN_SUCCESS);
export const loginFailure = action(authentication.LOGIN_FAILURE);
export const logout = action(authentication.USER_LOGOUT);

export const setMultiLanguage = action(authentication.SET_MULTI_LANGUAGE);
