// eslint-disable-next-line import/prefer-default-export
export const authentication = {
  REGISTER_REQUEST: 'REGISTER_REQUEST',
  REGISTER_SUCCESS: 'REGISTER_SUCCESS',
  REGISTER_FAILURE: 'REGISTER_FAILURE',

  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_FAILURE: 'LOGIN_FAILURE',

  SET_USER_INFO_SUCCESS: 'SET_USER_INFO_SUCCESS',
  USER_LOGOUT: 'USER_LOGOUT',

  SET_MULTI_LANGUAGE: 'SET_MULTI_LANGUAGE',
};
