// Method call API
export const METHOD = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

export const RADIAN = Math.PI / 180;

export const COLORS_PIECHART = ['#0088FE', '#00C49F', '#FFBB28', '#FF8042'];
export const STRING = {
  EMPTY: '',
  DOT: '.',
  COMMA: ',',
  QUESTION_MARK: '?',
  ENABLED: 'enabled',
  STATE: 'state',
  USER_NAME: 'username',
  PASSWORD: 'password',
  ACCESS_LEVEL: 'accessLevel',
  TOTAL: 'total',
  TODAY: 'Today',
  MONTH: 'month',
  DAY: 'day',
  IS_KEEP_LOGIN: 'isKeepLogin',
  BRAND: 'brand',
  OUTLET: 'outlet',
  TERMINAL: 'terminal',
};

export const OBJECT_TYPE = {
  NUMBER: 'number',
};

// Localstorage
export const LOGIN_LOCALSTORAGE_OBJECT = {
  LOGIN_SAVE_USE_NAME: 'userName',
  TOKEN_TIME_OUT: 'timeout',
  TOKEN: 'token',
  SIGNAL: 'signal',
  PUBLICKEY: 'PUBLICKEY',
  PERMISSION: 'permission',
};

export const POSITION = {
  FIRST: 0,
  SECOND: 1,
  THIRST: 2,
};

export const WIDTH_NAVIGATION = {
  TOGGLE_RESPONESIVE: '100px',
  NOT_TOGGLE_RESPONESIVE: '50vw',

  NOT_TOGGLE_NOT_RESPONESIVE: '112px',
  TOGGLE_NOT_RESPONESIVE: '112px',
  SUB_MENU_NOT_RESPONESIVE: '287px',
};
export const STATE = {
  DISABLE: 'DISABLE',
  ASSIGNED: 'ASSIGNED',
  AVAILABLE: 'AVAILABLE',
};

export const ACTIVE = {
  ON: {
    NAME: 'ON',
    VALUE: 1,
    NAME_DIFF: 'ACTIVE',
  },
  OFF: {
    NAME: 'OFF',
    VALUE: 0,
    NAME_DIFF: 'DISABLE',
  },
};

export const ACTIVE_SCHEME = {
  ON: {
    NAME: 'ON',
    VALUE: 1,
    TRUE: true,
  },
  OFF: {
    NAME: 'OFF',
    VALUE: 2,
    FALSE: false,
  },
};

export const TIME_OUT = {
  INTERVAL_CHECK_TOKEN: 1000,
  TOKEN_TIME_OUT: 1000,
  TIMEOUT_SLEEP_API: 500,
  INTERVAL_CHECK_EXTEND_TOKEN: 600000,
};

export const FORMAT_DATE = {
  TIME_BEGIN_DAY: ' 00:00:00',
  TIME_END_DAY: ' 23:59:59',
  DATE: 'DD/MM/YYYY',
  DATE_NO_SLASH: 'YYYYMMDD',
  DATE_TIME: 'DD/MM/YYYY HH:mm:ss',
};

export const INFORM = {
  FAIL: 'Error',
  WARNING: 'Warning',
  SUCCESS: 'Success',
};

export const ENV = {
  DEV: 'development',
  PRO: 'production',
};

export const SORT_TYPE = {
  ASC: 'asc',
  DESC: 'desc',
};

export const URL_DEFINE = {
  ID: ':id',
  Home: '/',
  Login: '/login',
  Admin: {
    Root: '/admin',
    Dashboard: '/admin',
    Users: '/admin/users',
    Reports: '/admin/reports',
  },
  OtherURL: '/*',
};

export const TYPE_REQUEST = {
  AUTHENTICATION: 'AUTHENTICATION',
  UNAUTHENTICATION: 'UNAUTHENTICATION',
};

export const USER_ROLE = {
  ROLE_ADMIN: 'ROLE_ADMIN',
  ROLE_ITEM: 'ROLE_ITEM',
  ROLE_REPORT: 'ROLE_REPORT',
};

/* Menu Define */
export const MENU_ADMIN = [];

export const TABLE_MANAGE = {
  pageSize: 10,
  pageOfItems: 1,
  totalItem: 0,
  loading: true,
};

/* Begin Manage Account */

export const ADD_ACCOUNT = {
  userId: 'UserID',
  password: 'Password',
  name: 'Name',
  email: 'E-Mail',
  department: 'Department',
  remarks: 'Remarks',
  accessLevel: 'Access Level',
  confirmPassword: 'Confirm Password',
};
/* End Manage Account */

export const CHECKBOX_STATUS = {
  ALL: 2,
  CHECKED: 1,
  UNCHECKED: 0,
};

export const COLORS_LINECHART = [
  {
    name: 'Color 1',
    hex: '#f8be28',
    color: {
      r: 248,
      g: 190,
      b: 40,
      a: 1,
    },
  },
  {
    name: 'Color 2',
    hex: '#5e8dfd',
    color: {
      r: 94,
      g: 141,
      b: 253,
      a: 1,
    },
  },
  {
    name: 'Color 3',
    hex: '#d0021c',
    color: {
      r: 208,
      g: 2,
      b: 28,
      a: 1,
    },
  },
  {
    name: 'Color 4',
    hex: '#7ed321',
    color: {
      r: 126,
      g: 211,
      b: 33,
      a: 1,
    },
  },
  {
    name: 'Color 5',
    hex: '#990099',
    color: {
      r: 153,
      g: 0,
      b: 153,
      a: 1,
    },
  },
  {
    name: 'Color 6',
    hex: '#3B3EAC',
    color: {
      r: 59,
      g: 62,
      b: 172,
      a: 1,
    },
  },
  {
    name: 'Color 7',
    hex: '#0099C6',
    color: {
      r: 0,
      g: 153,
      b: 198,
      a: 1,
    },
  },
  {
    name: 'Color 8',
    hex: '#DD4477',
    color: {
      r: 221,
      g: 68,
      b: 119,
      a: 1,
    },
  },
  {
    name: 'Color 9',
    hex: '#66AA00',
    color: {
      r: 102,
      g: 170,
      b: 0,
      a: 1,
    },
  },
  {
    name: 'Color 10',
    hex: '#B82E2E',
    color: {
      r: 184,
      g: 46,
      b: 46,
      a: 1,
    },
  },
  {
    name: 'Color 11',
    hex: '#316395',
    color: {
      r: 49,
      g: 99,
      b: 149,
      a: 1,
    },
  },
  {
    name: 'Color 12',
    hex: '#994499',
    color: {
      r: 153,
      g: 68,
      b: 153,
      a: 1,
    },
  },
  {
    name: 'Color 13',
    hex: '#22AA99',
    color: {
      r: 34,
      g: 170,
      b: 153,
      a: 1,
    },
  },
  {
    name: 'Color 14',
    hex: '#AAAA11',
    color: {
      r: 170,
      g: 170,
      b: 17,
      a: 1,
    },
  },
  {
    name: 'Color 15',
    hex: '#6633CC',
    color: {
      r: 102,
      g: 51,
      b: 204,
      a: 1,
    },
  },
  {
    name: 'Color 16',
    hex: '#E67300',
    color: {
      r: 230,
      g: 115,
      b: 0,
      a: 1,
    },
  },
  {
    name: 'Color 17',
    hex: '#8B0707',
    color: {
      r: 139,
      g: 7,
      b: 7,
      a: 1,
    },
  },
  {
    name: 'Color 18',
    hex: '#329262',
    color: {
      r: 50,
      g: 146,
      b: 98,
      a: 1,
    },
  },
  {
    name: 'Color 19',
    hex: '#5574A6',
    color: {
      r: 85,
      g: 116,
      b: 166,
      a: 1,
    },
  },
  {
    name: 'Color 20',
    hex: '#3B3EAC',
    color: {
      r: 59,
      g: 62,
      b: 172,
      a: 1,
    },
  },
];
