export const ERROR_MESSAGE = {
    AUTHEN: {
        CODE: 403,
        MESSAGE: ""
    },
    WRONG_PASSWORD: {
        CODE: 0,
        MESSAGE: "Please check password"
    },
    NOT_MATCH_PASSWORD: {
        CODE: 0,
        MESSAGE: "Password and confirm password does not match"
    },
    INSTANTIATION_FAIL: {
        CODE: 0,
        MESSAGE: "Error: Instantiation failed: Use Network.getInstance() instead of new."
    },
    NETWORK: {
        CODE: 0,
        MESSAGE: "Network Error !!"
    }
}

export const FORM_ERROR_MESSAGE = {
    Description: "Please input description",
    DeviceId: "Please input device Id",
    Name: "Please input name",
    UserId: "Please input user id",
    Department: "Please input department",
    Remarks: "Please input remarks",
    Phone: "Please input phone",
    Email: "Please input email",
    Address: "Please input address",
    UserName: "Please input user name",
    FullName: "Please input full name",
    PhoneNumber: "Please input phone number",
    Password: "Please input password",
    ConfirmPassword: "Please input confirm password",
    CurrentPassword: "Please input current password",
    NewPassword: "Please input new password",
    ShortName: "Please input short name",
    Quantity: "Please input quantity",
    UEN: "Please input UEN",
    BusinessAddress: "Please input business address",
    ContactPhone: "Please input contact phone number",
    BusinessName: "Please input business name",
    SingaporePostalCode: "Please input Singapore postal code",
    FaxNumber: "Please input fax number",
    PostalCode: "Please input postal code",
    Id: "Please input user id",
    Uen: "Please input uen",
}

export const FORM_VALIDATE_MESSAGE = {
    Email: "Invalid email format",
    Password: "Please enter password between 12 and 20 letter and must have at least 1 small-case letter, 1 capital letter, 1 digit, 1 special character",
    FullName: "Full name is only anphabet and less than 20 character",
    UserName: "User name between 6 and 30 character",
    UserId: "User id less than 20 character",
    Department: "Department is less than 100 character",
    Remark: "Remark is less than 100 character",
    Uen: "Uen is less than 20 character",
    BusinessName: "Business name is less than 20 character",
    BusinessAddress: "Business address is less than 50 character",
    PostalCode: "Postal code accepts only digits"
}

export const FORM_MESSAGE = {
    Validated: "look goods"
}

export const INFORM_MESSAGE = {
    Delete: "Are you delete ?",
    RemoveAssign: "Do you remove the terminal assignment: ",
    TerminalList: "Please check terminal list",
    AssignScheme: "Add Scheme Success",
    OK: "OK",
    ChangePasswordSuccess: "Change password successfully"
}
