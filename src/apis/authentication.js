import { service } from '../services/service.config';
import { API_LOGIN } from '../constants/APINames';
import { STRING, TYPE_REQUEST, METHOD } from '../constants/Const';

export function* registerApi(firstName, lastName, email, phone, password) {
  // try {
  //   const response = yield transporter('gateway').post('/auth/signup', {
  //     firstName,
  //     lastName,
  //     email,
  //     phone,
  //     password,
  //   });
  //   return response;
  // } catch (error) {
  //   console.error('API', error);
  //   return false;
  // }
}

export function* loginApi(username, password) {
  try {
    const formdata = new FormData();
    formdata.append(STRING.USER_NAME, username);
    formdata.append(STRING.PASSWORD, password);
    const response = yield service
      .sendRequest(TYPE_REQUEST.UNAUTHENTICATION, METHOD.POST, API_LOGIN, formdata)
      .then((res) => {
        console.log({ res });
        return res;
      });
    return response;
  } catch (error) {
    console.error('API', error);
    return false;
  }
}

// export function* fefreshTokenApi(refreshToken) {
//   try {
//     const response = yield transporter('base').post('/auth/token', {
//       refreshToken,
//     });
//     return response;
//   } catch (error) {
//     console.error('API', error);
//     return false;
//   }
// }
