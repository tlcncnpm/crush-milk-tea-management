import React from 'react';
import {
  Dashboard,
  People,
  BarChart,
  VpnKey,
  Category,
  LocalDrink,
  Fastfood,
  LocalGroceryStore,
  Notifications,
  HdrWeak,
  Style,
} from '@material-ui/icons';
import { URL_DEFINE } from '../constants/Const';

const SidebarItems = [
  {
    icon: <Dashboard />,
    title: 'Dashboard',
    path: URL_DEFINE.Admin.Dashboard,
    exact: true,
  },
  {
    icon: <People />,
    title: 'Users',
    path: URL_DEFINE.Admin.Users,
    exact: true,
  },
  {
    icon: <VpnKey />,
    title: 'Token',
    path: 'token',
    exact: true,
  },
  {
    icon: <Category />,
    title: 'Categorys',
    path: 'category',
    exact: true,
  },
  {
    icon: <LocalDrink />,
    title: 'Main Drink',
    path: 'maindrink',
    exact: true,
  },
  {
    icon: <Fastfood />,
    title: 'Drinks',
    path: 'drink',
    exact: true,
  },
  {
    icon: <LocalGroceryStore />,
    title: 'Orders',
    path: 'orders',
    exact: true,
  },
  {
    icon: <Notifications />,
    title: 'Notifications',
    path: 'notification',
    exact: true,
  },
  {
    icon: <BarChart />,
    title: 'Reports',
    path: URL_DEFINE.Admin.Reports,
    exact: true,
  },
  {
    icon: <HdrWeak />,
    title: 'Status',
    path: 'status',
    exact: true,
  },
  {
    icon: <Style />,
    title: 'Promotion',
    path: 'promotion',
    exact: true,
  },
];

export default SidebarItems;
