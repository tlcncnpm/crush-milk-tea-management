import ErrorPage403 from '../pages/Error/403';
import ErrorPage404 from '../pages/Error/404';
import ErrorPage500 from '../pages/Error/500';
import { URL_DEFINE } from '../constants/Const';
import Dashboard from '../pages/Admin/Dashboard';
import Users from '../pages/Admin/Users';
import Login from '../pages/Login';

export default [
  {
    path: URL_DEFINE.Home,
    component: Dashboard,
    // authority: true,
    authority: false,
    roles: ['ADMIN'],
    exact: true,
  },
  {
    path: URL_DEFINE.Login,
    component: Login,
    authority: false,
    exact: true,
    blankTemplate: true,
  },
  {
    path: URL_DEFINE.Admin.Root,
    component: Dashboard,
    // authority: true,
    authority: false,
    roles: ['ADMIN'],
    exact: true,
  },
  {
    path: URL_DEFINE.Admin.Dashboard,
    component: Dashboard,
    // authority: true,
    authority: false,
    roles: ['ADMIN'],
    exact: true,
  },
  {
    path: URL_DEFINE.Admin.Users,
    component: Users,
    // authority: true,
    authority: false,
    roles: ['ADMIN'],
    exact: true,
  },
  {
    path: '/403-error',
    component: ErrorPage403,
    authority: false,
  },
  {
    path: '/500-error',
    component: ErrorPage500,
    authority: false,
  },
  {
    path: URL_DEFINE.OtherURL,
    component: ErrorPage404,
    authority: false,
  },
];
