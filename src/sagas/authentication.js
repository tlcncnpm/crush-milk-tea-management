import { put, call, takeLatest } from 'redux-saga/effects';

import { authentication } from '../actions/types';
import {
  registerSuccess,
  registerFailure,
  loginSuccess,
  loginFailure,
} from '../actions/authentication';
import { loginApi, registerApi } from '../apis/authentication';

function* registerSagas(action) {
  try {
    const {
      firstName, lastName, email, phone, password,
    } = action.payload;
    const response = yield call(registerApi, firstName, lastName, email, phone, password);

    if (response && response.status === 200) {
      yield put(registerSuccess());
    } else if (response.problem === 'NETWORK_ERROR') {
      yield put(registerFailure());
      console.log(response.problem);
    } else {
      yield put(registerFailure());
      console.log(response.problem);
    }
  } catch (error) {
    console.error('Saga', error);
  }
}

function* loginSagas(action) {
  try {
    const { username, password } = action.payload;
    const response = yield call(loginApi, username, password);

    if (response && response.status === 200) {
      const { data } = response;
      yield put(loginSuccess(data));
    } else if (response.problem === 'NETWORK_ERROR') {
      yield put(loginFailure());
    } else {
      yield put(loginFailure());
      console.log(response.problem);
    }
  } catch (error) {
    console.error('Saga', error);
  }
}

export default function* watchAuthentication() {
  yield takeLatest(authentication.REGISTER_REQUEST, registerSagas);
  yield takeLatest(authentication.LOGIN_REQUEST, loginSagas);
}
