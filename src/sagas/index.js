import { all } from 'redux-saga/effects';
import auth from './authentication';

export default function* rootSaga() {
  yield all([auth()]);
}
