import React, { PureComponent } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ErrorPage403 from '../../pages/Error/403';

class AuthorizedRoute extends PureComponent {
  render() {
    const {
      isLoggedIn,
      component: Component,
      authority,
      render,
      roles,
      role,
      ...rest
    } = this.props;

    return (
      <Route
        {...rest}
        render={(props) => {
          if (authority) {
            if (isLoggedIn) {
              if (roles.length === 0 || roles.includes(role)) {
                return <Component {...props} />;
              }
              return <ErrorPage403 />;
            }
            return <Redirect to="/login" />;
          }
          return <Component {...props} />;
        }}
      />
    );
  }
}

AuthorizedRoute.propTypes = {
  exact: PropTypes.bool,
  authority: PropTypes.bool,
  isLoggedIn: PropTypes.bool.isRequired,
  path: PropTypes.string.isRequired,
  component: PropTypes.func.isRequired,
  roles: PropTypes.arrayOf(PropTypes.string),
};

AuthorizedRoute.defaultProps = {
  roles: [],
  authority: false,
  exact: false,
};

const mapStateToProps = state => ({
  isLoggedIn: state.auth.data.isLoggedIn,
  role: state.auth.data.role,
});

export default connect(mapStateToProps)(AuthorizedRoute);
