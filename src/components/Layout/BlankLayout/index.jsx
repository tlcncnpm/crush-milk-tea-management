import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../index.scss';

class BlankLayout extends Component {
  render() {
    const { component: Components } = this.props;
    return <Components props={this.props} />;
  }
}

BlankLayout.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string,
    PropTypes.element,
    PropTypes.array,
  ]).isRequired,
};

export default BlankLayout;
