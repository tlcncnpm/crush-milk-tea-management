import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import {
  CssBaseline,
  Drawer,
  AppBar,
  Toolbar,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Typography,
  Divider,
  IconButton,
  Badge,
  Container,
  Grid,
  Menu,
  MenuItem,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import AssignmentIcon from '@material-ui/icons/Assignment';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Footer from '../Footer';
import SidebarItems from '../../../configs/SidebarItems';
import './index.scss';

const mainListItems = SidebarItems.map((item, index) => (
  <NavLink key={index} to={item.path} exact={item.exact}>
    <ListItem button>
      <ListItemIcon>{item.icon}</ListItemIcon>
      <ListItemText primary={item.title} />
    </ListItem>
  </NavLink>
));

class AdminLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenDrawer: true,
      title: 'Admin',
      anchorEl: null,
    };
  }

  handleDrawerOpen = () => {
    this.setState({ isOpenDrawer: true });
  };

  handleDrawerClose = () => {
    this.setState({ isOpenDrawer: false });
  };

  handleSetTitlePage = (title) => {
    this.setState({ title });
  };

  handleUserMenuClick = (event) => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleUserMenuClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { component: Components } = this.props;
    const { isOpenDrawer, title, anchorEl } = this.state;
    return (
      <div className={`wrapper-admin-layout ${isOpenDrawer ? 'drawer-open' : 'drawer-close'}`}>
        <CssBaseline />
        <AppBar position="absolute">
          <Toolbar>
            <Typography component="h1" variant="h6" color="inherit" noWrap>
              <IconButton
                edge="start"
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerOpen}
                className="toggle-header-button"
              >
                <MenuIcon />
              </IconButton>
              {title}
            </Typography>
            <Typography>
              <IconButton color="inherit">
                <Badge badgeContent={4} color="secondary">
                  <NotificationsIcon />
                </Badge>
              </IconButton>
              <IconButton color="inherit" aria-owns="user-menu" onClick={this.handleUserMenuClick}>
                <AccountCircleIcon />
              </IconButton>
              <Menu
                id="user-menu"
                anchorEl={anchorEl}
                open={Boolean(anchorEl)}
                onClose={this.handleUserMenuClose}
              >
                <MenuItem>
                  <AssignmentIcon />
                  Profile
                </MenuItem>
                <MenuItem>
                  <ExitToAppIcon />
                  Logout
                </MenuItem>
              </Menu>
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" className="wrapper-nav-bar">
          <div className="logo-container">
            <div className="main-logo" />
            <IconButton onClick={this.handleDrawerClose} className="toggle-button">
              <ChevronLeftIcon />
            </IconButton>
          </div>
          <Divider />
          <List>{mainListItems}</List>
        </Drawer>
        <main className="admin-layout-content">
          <Container maxWidth="lg">
            <Grid container spacing={3}>
              <Components props={this.props} handleSetTitlePage={this.handleSetTitlePage} />
            </Grid>
          </Container>
          <Footer />
        </main>
      </div>
    );
  }
}

AdminLayout.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.string,
    PropTypes.element,
    PropTypes.array,
  ]).isRequired,
};

export default AdminLayout;
