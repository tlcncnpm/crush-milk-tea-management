import React, { Component } from 'react';
import { Typography, Link } from '@material-ui/core';

import './index.scss';

// const listContact = ['098774343', 'promo@icon.deals'];
// const listAboutUs = ['Về IconDeals', 'Hợp tác', 'Tuyển dụng', 'Thương hiệu'];
// const listTerms = ['Hỏi đáp', 'Điều khoản & quy định', 'Giao hàng'];

function MadeWithLove() {
  return (
    <Typography variant="body2" align="right">
      {'Copyright © 2019 '}
      <Link color="inherit" href="https://material-ui.com/">
        {'cruskmilktea'}
      </Link>
      {' team'}
    </Typography>
  );
}

class FooterLayout extends Component {
  render() {
    return (
      <footer className="wrapper-footer">
        <MadeWithLove />
      </footer>
    );
  }
}

export default FooterLayout;
