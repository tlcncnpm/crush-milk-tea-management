import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogTitle,
  DialogActions,
  Button,
  DialogContent,
  DialogContentText,
} from '@material-ui/core';

class Confirm extends Component {
  handleConfirm = () => {
    const { data, onConfirm } = this.props;
    onConfirm(data);
  };

  render() {
    const {
      open, title, content, onClose,
    } = this.props;
    return (
      <Dialog onClose={this.handleClose} open={open}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>
          <DialogContentText>{content}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleConfirm} color="primary" autoFocus>
            OK
          </Button>
          <Button onClick={onClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    );
  }
}

Confirm.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  title: PropTypes.string,
  content: PropTypes.string,
  data: PropTypes.any, // eslint-disable-line
};
Confirm.defaultProps = {
  title: '',
  content: '',
  data: null,
};
export default Confirm;
