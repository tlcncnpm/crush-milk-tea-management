import React, { Component } from 'react';
import { TextField, InputAdornment } from '@material-ui/core';

class CustomInput extends Component {
  render() {
    const {
      input,
      meta,
      hasFeedback,
      label,
      children,
      startIcon = <></>,
      endIcon = <></>,
      ...rest
    } = this.props;
    const hasError = meta.touched && meta.invalid;
    return (
      <TextField
        label={label}
        error={hasError}
        helperText={hasError && meta.error}
        {...rest}
        {...input}
        InputProps={{
          startAdornment: <InputAdornment position="start">{startIcon}</InputAdornment>,
          endAdornment: <InputAdornment position="end">{endIcon}</InputAdornment>,
        }}
      >
        {children}
      </TextField>
    );
  }
}

export default CustomInput;
