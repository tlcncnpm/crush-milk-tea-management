import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Dashboard extends Component {
  componentDidMount() {
    this.props.handleSetTitlePage('Dashboard'); // eslint-disable-line
  }

  render() {
    return <div>Dashboard</div>;
  }
}

Dashboard.propTypes = {
  handleSetTitlePage: PropTypes.func.isRequired,
};

export default Dashboard;
