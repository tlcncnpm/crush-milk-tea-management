import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, IconButton, InputBase } from '@material-ui/core';
import { Add as AddIcon, Search as SearchIcon } from '@material-ui/icons';
import UserList from './UserList';
import AddUserModal from './AddUserModal';
import Confirm from '../../../components/Confirm';

class Users extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdd: false,
      deleteId: null,
      initialData: null,
    };
  }

  componentDidMount() {
    this.props.handleSetTitlePage('Users Management'); // eslint-disable-line
  }

  handleDelete = (data) => {
    console.log(data);
    // TODO: call action delete
    this.setState({ deleteId: null });
  };

  handleOpenModal = (deleteId) => {
    if (deleteId) {
      this.setState({ deleteId });
    } else {
      this.setState({ isAdd: true, initialData: null });
    }
  };

  handleCloseModal = () => {
    this.setState({ isAdd: false, deleteId: null, initialData: null });
  };

  initEdit = (data) => {
    this.setState({
      initialData: data,
      isAdd: true,
    });
  };

  render() {
    const { isAdd, deleteId, initialData } = this.state;
    return (
      <React.Fragment>
        <AddUserModal
          open={isAdd}
          initialData={initialData}
          title="Add User"
          onClose={this.handleCloseModal}
        />
        <Confirm
          open={!!deleteId}
          title="Delete User"
          content="Are you sure to delete?"
          data={deleteId}
          onClose={this.handleCloseModal}
          onConfirm={this.handleDelete}
        />
        <div className="table-control-container">
          <div>
            <InputBase placeholder="Search" />
            <IconButton>
              <SearchIcon />
            </IconButton>
          </div>
          <Button variant="contained" color="primary" onClick={() => this.handleOpenModal(null)}>
            <AddIcon />
            {' Add'}
          </Button>
        </div>
        <UserList onOpenModal={this.handleOpenModal} initEdit={this.initEdit} />
      </React.Fragment>
    );
  }
}

Users.propTypes = {
  handleSetTitlePage: PropTypes.func.isRequired,
};

export default Users;
