import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button, IconButton, DialogContent, DialogActions,
} from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { Field, reduxForm } from 'redux-form';
import CustomInput from '../../../../../components/Form/CustomInput';
import validate from './validate';

class AddUserForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typePassword: true,
    };
  }

  componentDidMount() {
    const { initialData } = this.props;
    if (initialData) {
      this.handleInitialize(initialData);
    }
  }

  handleInitialize = (initialData) => {
    const initData = {
      username: initialData.username,
    };
    this.props.initialize(initData); // eslint-disable-line
  };

  /**
   * handle change typePassword state
   */
  onChangeTypePassword = () => {
    // eslint-disable-next-line
    this.setState({ typePassword: !this.state.typePassword });
  };

  render() {
    const {
      handleSubmit, isFetching: submitting, pristine, invalid, onClose,
    } = this.props;
    const { typePassword } = this.state;
    return (
      <form noValidate onSubmit={handleSubmit}>
        <DialogContent dividers>
          <Field
            name="username"
            type="text"
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="User name"
            autoComplete="text"
            component={CustomInput}
            disabled={submitting}
            hasFeedback
            autoFocus
          />
          <Field
            name="password"
            type={typePassword ? 'password' : 'text'}
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Password"
            endIcon={(
              <IconButton
                aria-label="Toggle password visibility"
                onClick={this.onChangeTypePassword}
              >
                {typePassword ? <VisibilityOff /> : <Visibility />}
              </IconButton>
)}
            autoComplete="current-password"
            component={CustomInput}
            disabled={submitting}
            hasFeedback
          />
          <Field
            name="email"
            type="text"
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Email"
            autoComplete="text"
            component={CustomInput}
            disabled={submitting}
            hasFeedback
          />
          <Field
            name="phoneNumber"
            type="text"
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Phone Number"
            autoComplete="text"
            component={CustomInput}
            disabled={submitting}
            hasFeedback
          />
          <Field
            name="address"
            type="text"
            variant="outlined"
            margin="dense"
            required
            fullWidth
            label="Address"
            autoComplete="text"
            component={CustomInput}
            disabled={submitting}
            hasFeedback
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={this.handleSubmit}
            color="primary"
            type="submit"
            disabled={submitting || invalid || pristine}
          >
            Save
          </Button>
          <Button onClick={onClose}>Close</Button>
        </DialogActions>
      </form>
    );
  }
}

AddUserForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
  initialData: PropTypes.instanceOf(Object),
};
AddUserForm.defaultProps = {
  initialData: null,
};
export default reduxForm({
  form: 'AddUserForm',
  validate,
})(AddUserForm);
