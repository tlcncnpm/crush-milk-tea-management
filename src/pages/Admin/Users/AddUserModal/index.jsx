import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dialog, IconButton, Typography } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import AddUserForm from './AddUserForm';

const DialogTitle = (props) => {
  const { title, onClose } = props;
  return (
    <MuiDialogTitle disableTypography>
      <Typography variant="h6">{title}</Typography>
      {onClose ? (
        <IconButton onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
};

DialogTitle.propTypes = {
  title: PropTypes.string.isRequired,
  onClose: PropTypes.func,
};
DialogTitle.defaultProps = {
  onClose: null,
};

class AddUserModal extends Component {
  handleClose = () => {
    this.props.onClose(); // eslint-disable-line
  };

  handleSubmit = (result) => {
    console.log(result);
    // TODO: call action add/edit
  };

  render() {
    const { open, title, initialData } = this.props;
    return (
      <Dialog onClose={this.handleClose} open={open} className="wrapperAddTheme">
        <DialogTitle onClose={this.handleClose} title={title} />
        <AddUserForm
          onSubmit={this.handleSubmit}
          isFetching={false}
          onClose={this.handleClose}
          initialData={initialData}
        />
      </Dialog>
    );
  }
}

AddUserModal.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string,
  initialData: PropTypes.instanceOf(Object),
};
AddUserModal.defaultProps = {
  title: '',
  initialData: null,
};
export default AddUserModal;
