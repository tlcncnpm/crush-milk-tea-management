import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

class HomePage extends PureComponent {
  render() {
    return <div>Home page</div>;
  }
}

function mapStateToProps() {
  return {};
}

export default connect(mapStateToProps)(HomePage);
