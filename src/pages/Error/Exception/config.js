const config = {
  403: {
    img: '/assets/images/error-pages/403.svg',
    title: '403',
    desc: 'Forbidden Error',
  },
  404: {
    img: '/assets/images/error-pages/404.svg',
    title: '404',
    desc: 'Page Not Found',
  },
  500: {
    img: '/assets/images/error-pages/500.svg',
    title: '500',
    desc: 'Internal Server Error',
  },
};

export default config;
