import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  CssBaseline, Box, Typography, Container, Avatar,
} from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import LoginForm from './LoginForm';

import { loginRequest } from '../../actions/authentication';

import ApiStatus from '../../utils/apiStatus';

import './index.scss';

class Login extends Component {
  handleSubmit = (result) => {
    this.props.loginRequest(result); // eslint-disable-line react/destructuring-assignment
  };

  render() {
    const { multiLang, login, isLoggedIn } = this.props;
    const { status } = login;
    const isFetching = status === ApiStatus.FETCHING;

    if (!multiLang) return null;
    if (isLoggedIn) {
      return <Redirect to="/admin" />;
    }
    return (
      <div className="wrapper-login">
        <div className="opacity-background" />
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <div className="login-box">
            <Avatar>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Login in
            </Typography>
            <LoginForm onSubmit={this.handleSubmit} isFetching={isFetching} />
            <Box mt={5}>
              <MadeWithLove />
            </Box>
          </div>
        </Container>
      </div>
    );
  }
}

Login.propTypes = {
  multiLang: PropTypes.func,
  login: PropTypes.instanceOf(Object).isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
  loginRequest: PropTypes.func.isRequired,
};

Login.defaultProps = {
  multiLang: null,
};

function mapStateToProps(state) {
  const { auth } = state;
  const { multiLang, login } = auth;
  const { isLoggedIn } = auth.data;

  return { multiLang, login, isLoggedIn };
}

const mapActionToProps = {
  loginRequest,
};

export default connect(
  mapStateToProps,
  mapActionToProps,
)(Login);

function MadeWithLove() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Built with love by the '}
      <Link color="inherit" to="https://material-ui.com/">
        crushmilktea
      </Link>
      {' team.'}
    </Typography>
  );
}
