import validator from 'validator';

export default function (values) {
  const errors = {};

  if (!values.username) {
    errors.username = 'Please type your username';
  }

  if (!values.password) {
    errors.password = 'Please type your password';
  } else if (!validator.isLength(values.password, { min: 6, max: 20 })) {
    errors.password = 'Passwords must be between 6 and 20 characters';
  }

  return errors;
}
