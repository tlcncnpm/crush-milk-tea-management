import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Button, FormControlLabel, Checkbox, Grid, IconButton,
} from '@material-ui/core';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import { Field, reduxForm } from 'redux-form';
import CustomInput from '../../../components/Form/CustomInput';
import validate from './validate';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typePassword: true,
    };
  }

  /**
   * handle change typePassword state
   */
  onChangeTypePassword = () => {
    // eslint-disable-next-line
    this.setState({ typePassword: !this.state.typePassword });
  };

  render() {
    const {
      handleSubmit, pristine, isFetching: submitting, invalid,
    } = this.props;
    const { typePassword } = this.state;
    return (
      <form noValidate onSubmit={handleSubmit}>
        <Field
          name="username"
          type="text"
          variant="outlined"
          margin="normal"
          required
          fullWidth
          label="User name"
          autoComplete="text"
          component={CustomInput}
          disabled={submitting}
          hasFeedback
          autoFocus
        />
        <Field
          name="password"
          type={typePassword ? 'password' : 'text'}
          variant="outlined"
          margin="normal"
          required
          fullWidth
          label="Password"
          endIcon={(
            <IconButton aria-label="Toggle password visibility" onClick={this.onChangeTypePassword}>
              {typePassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
)}
          autoComplete="current-password"
          component={CustomInput}
          disabled={submitting}
          hasFeedback
        />
        <FormControlLabel
          control={<Checkbox value="remember" color="primary" />}
          label="Remember me"
        />
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
          loading={submitting.toString()}
          disabled={invalid || pristine}
        >
          Login In
        </Button>
        <Grid container>
          <Grid item>
            <Link to="/forgot">Forgot password?</Link>
          </Grid>
          <Grid item>
            {/* eslint-disable-next-line react/no-unescaped-entities */}
            <Link to="/sign-up">Don't have an account? Sign Up</Link>
          </Grid>
        </Grid>
      </form>
    );
  }
}

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  invalid: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'LoginForm',
  validate,
})(LoginForm);
