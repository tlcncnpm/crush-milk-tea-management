import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';

import english from './en';
import vietnamese from './vi';

i18n.use(LanguageDetector).init({
  resources: {
    en: english,
    vi: vietnamese,
  },
  fallbackLng: 'vi',
  debug: process.env.NODE_ENV !== 'production',

  // have a common namespace used around the full app
  ns: 'translation',
  defaultNS: 'translation',

  keySeparator: false, // we use content as keys

  interpolation: {
    escapeValue: false, // not needed for react!!
    formatSeparator: ',',
  },

  react: {
    wait: true,
  },
});

export default i18n;
