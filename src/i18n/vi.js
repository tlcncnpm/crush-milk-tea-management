export default {
  translation: {
    // Languages
    vietnamese: 'Tiếng Việt',
    english: 'Tiếng Anh',

    or: 'Hoặc',

    // Register
    register_by_facebook: 'Đăng ký với Facebook',
    first_name: 'Tên',
    last_name: 'Họ và Tên đệm',
    email: 'Địa chỉ email',
    phone: 'Số điện thoại',
    password: 'Mật khẩu',
    register: 'Đăng ký',
    allow_term: 'Bằng việc đăng kí, bạn đã đồng ý với IconDeals về',
    terms: 'Điều khoản sử dụng',
    already_have_account: 'Bạn đã có tài khoản',
    register_success_message:
            'Bạn đã đăng ký tài khoản thành công. Vui lòng đăng nhập để tiếp tục sử dụng.',
    register_success: 'Đăng ký thành công',
    create_new_account: 'Tạo tài khoản mới',

    // Login
    login: 'Đăng nhập',
    login_by_facebook: 'Đăng nhập với Facebook',
    not_have_account_yet: 'Bạn chưa có tài khoản?',
  },
};
