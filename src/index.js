import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { I18nextProvider } from 'react-i18next';
import { PersistGate } from 'redux-persist/integration/react';
import i18n from './i18n';
import AppRouter from './router';
import { store, persistor } from './store';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Provider store={store}>
    <PersistGate persistor={persistor}>
      <I18nextProvider initialLanguage="vi" i18n={i18n}>
        <AppRouter />
      </I18nextProvider>
    </PersistGate>
  </Provider>,
  document.getElementById('root'),
);
registerServiceWorker();
