import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withNamespaces } from 'react-i18next';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { routes } from '../configs';
import ErrorPage404 from '../pages/Error/404';
import AuthorizedRoute from '../components/Authorized';
import { setMultiLanguage } from '../actions/authentication';
import AdminLayout from '../components/Layout/AdminLayout';
import BasicLayout from '../components/Layout/BasicLayout';
import BlankLayout from '../components/Layout/BlankLayout';
import { setService } from '../services/service.config';

class AppRouter extends PureComponent {
  componentDidMount() {
    const { t, setMultiLanguage: setMultiLanguageAction } = this.props;
    setMultiLanguageAction({ t });
  }

  renderLayout = (item, props) => {
    if (item.blankTemplate) return <BlankLayout {...item} {...props} />;
    if (item.roles && item.roles.length) return <AdminLayout {...item} {...props} />;
    return <BasicLayout {...item} {...props} />;
  };

  renderRoute = () =>
    routes.map((item, index) => (
      <AuthorizedRoute
        key={index}
        path={item.path}
        roles={item.roles}
        exact={!!item.exact}
        component={props => this.renderLayout(item, props)}
        authority={item.authority !== false}
      />
    ));

  render() {
    const mode = process.env.NODE_ENV;
    setService(mode);
    return (
      <BrowserRouter>
        <Switch>
          {this.renderRoute()}
          <Route component={ErrorPage404} />
        </Switch>
      </BrowserRouter>
    );
  }
}

const mapActionToProps = {
  setMultiLanguage,
};

AppRouter.propTypes = {
  t: PropTypes.func.isRequired,
  setMultiLanguage: PropTypes.func.isRequired,
};

export default compose(
  connect(
    null,
    mapActionToProps,
  ),
  withNamespaces(),
)(AppRouter);
