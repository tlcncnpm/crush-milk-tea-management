import { ERROR_MESSAGE } from '../constants/Message';

class LocalStorage {
  static instance = new LocalStorage();

  constructor() {
    if (LocalStorage.instance) {
      throw new Error(ERROR_MESSAGE.INSTANTIATION_FAIL.MESSAGE);
    }
    LocalStorage.instance = this;
  }

  static getInstance() {
    return LocalStorage.instance;
  }

  /**
   * Get value with key into the Web Storage
   * @name get
   * @param {string} key
   * @return {*} value of the `key`
   */
  // eslint-disable-next-line class-methods-use-this
  get(key) {
    const result = localStorage[key];
    return result;
  }

  /**
   * Add value with key into the Web Storage
   * @name set
   * @param {string} key
   * @param {object, string} value
   */
  // eslint-disable-next-line class-methods-use-this
  set(key, value) {
    localStorage[key] = value;
  }

  /**
   * Delete the corresponding entry inside the Web Storage.
   * @name remove
   * @param {string} key
   */
  // eslint-disable-next-line class-methods-use-this
  remove(key) {
    delete localStorage[key];
  }

  /**
   * Clear the Storage in one go
   * @name removeAll
   */
  // eslint-disable-next-line class-methods-use-this
  removeAll() {
    localStorage.clear();
  }
}
const localStorageService = LocalStorage.getInstance();
export default localStorageService;
