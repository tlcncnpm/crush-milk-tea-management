import { ERROR_MESSAGE } from '../constants/Message';

class SessionStorage {
  static instance = new SessionStorage();

  constructor() {
    if (SessionStorage.instance) {
      throw new Error(ERROR_MESSAGE.INSTANTIATION_FAIL.MESSAGE);
    }
    SessionStorage.instance = this;
  }

  static getInstance() {
    return SessionStorage.instance;
  }

  /**
   * Get value with key into the Web Storage
   * @name get
   * @param {string} key
   * @return {*} value of the `key`
   */
  // eslint-disable-next-line class-methods-use-this
  get(key) {
    const result = sessionStorage[key];
    return result;
  }

  /**
   * Add value with key into the Web Storage
   * @name set
   * @param {string} key
   * @param {object, string} value
   */
  // eslint-disable-next-line class-methods-use-this
  set(key, value) {
    sessionStorage[key] = value;
  }

  /**
   * Delete the corresponding entry inside the Web Storage.
   * @name remove
   * @param {string} key
   */
  // eslint-disable-next-line class-methods-use-this
  remove(key) {
    delete sessionStorage[key];
  }

  /**
   * Clear the Storage in one go
   * @name removeAll
   */
  // eslint-disable-next-line class-methods-use-this
  removeAll() {
    sessionStorage.clear();
  }
}
const sessionStorageService = SessionStorage.getInstance();
export default sessionStorageService;
