import axios from 'axios';
import { encryptStringToBase64 } from '../utils/encrypt';
import sessionStorageService from './sessionStorageService';
import { decryptBase64ToString, parseJwt } from '../utils/decrypt';
import localStorageService from './localStorageService';
import { ERROR_MESSAGE } from '../constants/Message';
import {
  ENV,
  TIME_OUT,
  LOGIN_LOCALSTORAGE_OBJECT,
  TYPE_REQUEST,
  STRING,
  POSITION,
} from '../constants/Const';

let BASE_URL = STRING.EMPTY;

/**
 * Set environment
 * @param env : Environment name
 */
function setService(env) {
  if (env === ENV.DEV) {
    BASE_URL = process.env.REACT_APP_BASE_URL_DEV;
  } else if (env === ENV.PRO) {
    BASE_URL = process.env.REACT_APP_BASE_URL_PRO;
  } else {
    BASE_URL = STRING.EMPTY;
  }
}

class Network {
  static instance = new Network();

  token = STRING.EMPTY;

  constructor() {
    if (Network.instance) {
      throw new Error(ERROR_MESSAGE.INSTANTIATION_FAIL.MESSAGE);
    }
    Network.instance = this;
  }

  static getInstance() {
    return Network.instance;
  }

  setToken(token, isKeepLogin) {
    this.token = token;
    const tokenArray = token.split(STRING.DOT);
    if (isKeepLogin) {
      localStorageService.set(
        LOGIN_LOCALSTORAGE_OBJECT.TOKEN,
        encryptStringToBase64(
          tokenArray[POSITION.FIRST] + STRING.DOT + tokenArray[POSITION.SECOND],
        ),
      );
      localStorageService.set(
        LOGIN_LOCALSTORAGE_OBJECT.SIGNAL,
        encryptStringToBase64(tokenArray[POSITION.THIRST]),
      );
    } else {
      sessionStorageService.set(
        LOGIN_LOCALSTORAGE_OBJECT.TOKEN,
        encryptStringToBase64(
          tokenArray[POSITION.FIRST] + STRING.DOT + tokenArray[POSITION.SECOND],
        ),
      );
      sessionStorageService.set(
        LOGIN_LOCALSTORAGE_OBJECT.SIGNAL,
        encryptStringToBase64(tokenArray[POSITION.THIRST]),
      );
    }
    const dataToken = parseJwt(token);
    if (dataToken.exp > 0) {
      localStorageService.set(
        LOGIN_LOCALSTORAGE_OBJECT.TOKEN_TIME_OUT,
        (new Date().getTime() + dataToken.exp) * 1000 + STRING.EMPTY,
      );
    } else {
      localStorageService.set(LOGIN_LOCALSTORAGE_OBJECT.TOKEN_TIME_OUT, '-1');
    }
  }

  getToken() {
    if (!this.token) {
      if (localStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.TOKEN)) {
        this.token = decryptBase64ToString(
          localStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.TOKEN)
            ? localStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.TOKEN)
            : STRING.EMPTY,
        );
        this.token
          += STRING.DOT
          + decryptBase64ToString(
            localStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.SIGNAL)
              ? localStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.SIGNAL)
              : STRING.EMPTY,
          );
      } else {
        this.token = decryptBase64ToString(
          sessionStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.TOKEN),
        );
        this.token
          += STRING.DOT
          + decryptBase64ToString(sessionStorageService.get(LOGIN_LOCALSTORAGE_OBJECT.SIGNAL));
      }
    }
    return this.token;
  }

  /**
   * sendRequest
   * @param method : GET / POST / PUT / DELETE
   * @param endpoint : URL
   * @param body : JSON object
   * @returns {Promise<AxiosResponse<any> | never>}
   */
  sendRequest(type, method, url, data = {}, header = STRING.EMPTY) {
    // eslint-disable-next-line no-param-reassign
    header = Object.assign(
      header,
      type === TYPE_REQUEST.AUTHENTICATION ? { Authorization: this.getToken() } : STRING.EMPTY,
      header['Content-Type'] ? '' : { 'Content-Type': 'application/json' },
    );
    return axios({
      method,
      url,
      baseURL: BASE_URL,
      data,
      timeout: TIME_OUT.INTERVAL_REQUEST_SERVER,
      headers: {
        ...header,
        // 'Access-Control-Allow-Origin': '*',
      },
    }).then((res) => {
      if (res.data.errorCode && res.data.errorCode === ERROR_MESSAGE.AUTHEN.CODE) {
        localStorageService.removeAll();
        return res;
      }
      return res;
    });
  }
}

const service = Network.getInstance();
export { setService, service };
