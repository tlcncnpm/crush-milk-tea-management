import { combineReducers } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import auth from './authentication';

const persistConfig = {
  key: 'auth',
  storage,
  whitelist: ['data'],
};

const authReducer = persistReducer(persistConfig, auth);

export default combineReducers({
  form: reduxFormReducer,
  auth: authReducer,
});
