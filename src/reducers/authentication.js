import update from 'immutability-helper';
import { authentication } from '../actions/types';
import ApiStatus from '../utils/apiStatus';

const initialState = {
  data: {
    accessToken: null,
    info: null,
    isLoggedIn: false,
    roles: [],
  },
  register: ApiStatus.default(),
  login: ApiStatus.default(),
  multiLang: null,
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case authentication.LOGIN_REQUEST:
      return update(state, {
        login: { $set: ApiStatus.fetching() },
      });
    case authentication.LOGIN_FAILURE: {
      return update(state, {
        login: { $set: ApiStatus.failure() },
      });
    }
    case authentication.LOGIN_SUCCESS: {
      const { result } = action.payload;
      return update(state, {
        login: { $set: ApiStatus.success() },
        data: {
          encodedPrivateKey: { $set: result.encodedPrivateKey },
          accessToken: { $set: result.jwt },
          role: { $set: result.role },
          isLoggedIn: { $set: true },
        },
      });
    }

    case authentication.REGISTER_REQUEST:
      return update(state, {
        register: { $set: ApiStatus.fetching() },
      });
    case authentication.REGISTER_FAILURE: {
      return update(state, {
        register: { $set: ApiStatus.failure() },
      });
    }
    case authentication.REGISTER_SUCCESS: {
      return update(state, {
        register: { $set: ApiStatus.success() },
      });
    }

    case authentication.USER_LOGOUT: {
      return update(state, {
        data: {
          encodedPrivateKey: { $set: null },
          accessToken: { $set: null },
          role: { $set: null },
          isLoggedIn: { $set: false },
        },
      });
    }
    case authentication.SET_USER_INFO_SUCCESS: {
      const { userInfo } = action.payload;
      return update(state, {
        data: {
          info: { $set: userInfo },
        },
      });
    }
    case authentication.SET_MULTI_LANGUAGE: {
      const { t } = action.payload;
      return update(state, {
        multiLang: { $set: t },
      });
    }
    default:
      return state;
  }
}
